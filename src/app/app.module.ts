/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainNavigationComponent } from '@common-components/main-navigation/main-navigation.component';
import { VersionInfoComponent } from '@common-components/version-info/version-info.component';
import { SessionContext } from '@common/session-context';
import { LogoutComponent } from '@dialogs/logout/logout.component';
import { HttpCatchInterceptor } from '@interceptors/http-catch.interceptor';
import { LoginComponent } from '@pages/login/login.component';
import { ModuleGridComponent } from '@pages/module-grid/module-grid.component';
import { OverviewComponent } from '@pages/overview/overview.component';
import { AuthenticationService } from '@services/authentication.service';
import { UserModuleService } from '@services/user-module.service';
import { VersionInfoService } from '@services/version-info.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OverviewComponent,
    LogoutComponent,
    VersionInfoComponent,
    MainNavigationComponent,
    ModuleGridComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatIconModule,
    MatDialogModule,
  ],
  providers: [
    AuthenticationService,
    UserModuleService,
    VersionInfoService,
    SessionContext,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpCatchInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
