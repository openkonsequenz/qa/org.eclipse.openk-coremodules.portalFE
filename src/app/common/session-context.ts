/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Globals } from '@common/globals';
import { JwtPayload } from '@model/jwt-payload';
import { Settings } from 'app/model/settings';

@Injectable({
  providedIn: 'root',
})
export class SessionContext {
  public centralHttpResultCode$: EventEmitter<number> = new EventEmitter<number>();
  public settings: Settings = new Settings();

  constructor(private readonly router: Router) {}

  public getCurrSessionId(): string | null {
    const sid = localStorage.getItem(Globals.LOCALSTORAGE_SESSION_ID);
    if (sid) {
      return sid;
    } else {
      return '';
    }
  }
  public setCurrSessionId(sid: string): void {
    localStorage.setItem(Globals.LOCALSTORAGE_SESSION_ID, sid);
  }

  public clearStorage(): void {
    localStorage.clear();
  }

  public getAccessToken(): string | null {
    return localStorage.getItem(Globals.ACCESS_TOKEN);
  }

  public setAccessToken(accessToken: string): void {
    localStorage.setItem(Globals.ACCESS_TOKEN, accessToken);
  }

  public getAccessTokenDecoded(): JwtPayload | null {
    const accessToken = this.getAccessToken();
    if (!accessToken) {
      this.router.navigate(['/login']);
      throw new Error('No access token found');
    }
    const jwtHelper: JwtHelperService = new JwtHelperService();
    const decoded = jwtHelper.decodeToken(accessToken);
    const now = new Date();
    const nowTime = now.getTime();

    if (!decoded || decoded.exp > nowTime) {
      this.router.navigate(['/login']);
      throw new Error('Decoded Access Token is undefined or expired');
    }

    const jwtPayload: JwtPayload = new JwtPayload();
    jwtPayload.roles = [];
    jwtPayload.name = decoded.name;

    // add realm roles to jwtPayload.roles
    if (decoded.realm_access && decoded.realm_access.roles) {
      jwtPayload.roles = decoded.realm_access.roles;
    }

    // add client roles to jwtPayload.roles
    const clientRoles = [];
    const resourceAccessList = decoded.resource_access;
    for (const el in resourceAccessList) {
      if (Object.prototype.hasOwnProperty.call(resourceAccessList, el)) {
        clientRoles.push(...resourceAccessList[el].roles);
      }
    }
    jwtPayload.roles.push(...clientRoles);

    return jwtPayload;
  }
}
