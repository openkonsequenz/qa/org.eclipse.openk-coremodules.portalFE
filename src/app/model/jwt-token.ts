export class JwtToken {
  access_token: string = '';
  refresh_token: string = '';
  token_type: string = '';
  session_state: string = '';
  expires_in: number = 0;
  refresh_expires_in: number = 0;
}
