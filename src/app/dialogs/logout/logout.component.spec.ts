/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SessionContext } from '@common/session-context';
import { AuthenticationService } from '@services/authentication.service';
import { of } from 'rxjs';
import { LogoutComponent } from './logout.component';

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let authenticationService: AuthenticationService;
  let router: Router;
  let sessionContext: SessionContext;
  let matDialogRef: MatDialogRef<LogoutComponent>;

  beforeEach(async () => {
    const mockAuthService = { logout: jasmine.createSpy('logout') };
    const mockRouter = { navigate: jasmine.createSpy('navigate') };
    const mockSessionContext = {
      clearStorage: jasmine.createSpy('clearStorage'),
    };
    const mockDialogRef = { close: jasmine.createSpy('close') };

    await TestBed.configureTestingModule({
      declarations: [LogoutComponent],
      providers: [
        { provide: AuthenticationService, useValue: mockAuthService },
        { provide: Router, useValue: mockRouter },
        { provide: SessionContext, useValue: mockSessionContext },
        { provide: MatDialogRef, useValue: mockDialogRef },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    authenticationService = TestBed.inject(AuthenticationService);
    router = TestBed.inject(Router);
    sessionContext = TestBed.inject(SessionContext);
    matDialogRef = TestBed.inject(MatDialogRef);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should logout', () => {
    (authenticationService.logout as jasmine.Spy).and.returnValue(of({}));
    component.logout();
    expect(authenticationService.logout).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
    expect(sessionContext.clearStorage).toHaveBeenCalled();
    if (component.dialogRef) {
      expect(matDialogRef.close).toHaveBeenCalled();
    }
  });
});
