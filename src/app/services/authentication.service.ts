/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { JwtToken } from '@model/jwt-token';
import { LoginCredentials } from '@model/login-credentials';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BaseHttpService } from './base-http.service';

@Injectable()
export class AuthenticationService extends BaseHttpService {
  constructor(
    private http: HttpClient,
    private sessionContext: SessionContext
  ) {
    super();
  }

  public login(creds: LoginCredentials): Observable<JwtToken> {
    const headers = this.createCommonHeaders(this.sessionContext);

    return this.http
      .post<JwtToken>(Globals.BASE_PORTAL_URL + '/login', creds, { headers: headers, observe: 'response' })
      .pipe(
        map((response: HttpResponse<JwtToken>) => {
          super.extractSessionId(response.headers, this.sessionContext);

          return super.extractData<JwtToken>(response, this.sessionContext);
        }),
        catchError((err: unknown) => super.handleErrorPromise(err, this.sessionContext))
      );
  }

  public checkAuth(): Observable<{ ret: string }> {
    const headers = this.createCommonHeaders(this.sessionContext);

    return this.http
      .get<{ ret: string }>(Globals.BASE_PORTAL_URL + '/checkAuth', { headers: headers, observe: 'response' })
      .pipe(
        map((response: HttpResponse<{ ret: string }>) => super.extractData(response, this.sessionContext)),
        catchError((err: unknown) => super.handleErrorPromise(err, this.sessionContext))
      );
  }

  public logout(): Observable<{ ret: string }> {
    const headers = this.createCommonHeaders(this.sessionContext);

    return this.http
      .get<{ ret: string }>(Globals.BASE_PORTAL_URL + '/logout', { headers: headers, observe: 'response' })
      .pipe(
        map((response: HttpResponse<{ ret: string }>) => super.extractData(response, this.sessionContext)),
        catchError((err: unknown) => super.handleErrorPromise(err, this.sessionContext))
      );
  }
}
