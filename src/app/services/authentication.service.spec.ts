/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Globals } from '@common/globals';
import { SessionContext } from '@common/session-context';
import { JwtToken } from '@model/jwt-token';
import { LoginCredentials } from '@model/login-credentials';
import { JWT_TOKEN_HUGO } from '@testing/jwt-token';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService, SessionContext],
    });

    service = TestBed.inject(AuthenticationService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify(); // Ensure that there are no outstanding requests.
  });

  it('should call login with correct URL and headers', () => {
    const mockJwtToken: JwtToken = JWT_TOKEN_HUGO;
    const mockCreds: LoginCredentials = { userName: 'test', password: 'password' };

    service.login(mockCreds).subscribe(token => {
      expect(token).toEqual(mockJwtToken);
    });

    const req = httpTestingController.expectOne(Globals.BASE_PORTAL_URL + '/login');
    expect(req.request.method).toEqual('POST');
    expect(req.request.body).toEqual(mockCreds);

    req.flush(mockJwtToken);
  });

  it('should call checkAuth with correct URL and headers', () => {
    const mockResponse = { ret: 'mockResponse' };

    service.checkAuth().subscribe(response => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(Globals.BASE_PORTAL_URL + '/checkAuth');
    expect(req.request.method).toEqual('GET');

    req.flush(mockResponse);
  });

  it('should call logout with correct URL and headers', () => {
    const mockResponse = { ret: 'mockResponse' };

    service.logout().subscribe(response => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(Globals.BASE_PORTAL_URL + '/logout');
    expect(req.request.method).toEqual('GET');

    req.flush(mockResponse);
  });
});
