/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { OverviewComponent } from '@pages/overview/overview.component';
import { AuthenticationService } from '@services/authentication.service';
import { LoginCredentials } from 'app/model/login-credentials';
import { MockComponent } from 'app/testing/mock.component';
import { of } from 'rxjs';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthenticationService;

  beforeEach(async () => {
    const authServiceMock = {
      login: jasmine.createSpy('login').and.returnValue(of({})),
    };

    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'overview', component: OverviewComponent }])],
      declarations: [LoginComponent, MockComponent({ selector: 'app-version-info' })],
      providers: [{ provide: AuthenticationService, useValue: authServiceMock }],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthenticationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call login on the authentication service when login is called', () => {
    const credentials = new LoginCredentials();
    credentials.userName = 'test';
    credentials.password = 'password';
    component.login(credentials.userName, credentials.password);
    expect(authService.login).toHaveBeenCalledWith(credentials);
  });
});
