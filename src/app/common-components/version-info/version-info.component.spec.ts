/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Globals } from '@common/globals';
import { VersionInfo } from '@model/version-info';
import { VersionInfoService } from '@services/version-info.service';
import { of } from 'rxjs';
import { VersionInfoComponent } from './version-info.component';

describe('VersionInfoComponent', () => {
  let component: VersionInfoComponent;
  let fixture: ComponentFixture<VersionInfoComponent>;
  let versionInfoService: VersionInfoService;

  beforeEach(async () => {
    const versionInfoServiceMock = {
      loadBackendServerInfo: jasmine.createSpy('loadBackendServerInfo').and.returnValue(
        of({
          frontendVersion: Globals.FRONTEND_VERSION,
          backendVersion: '1.0.0',
          dbVersion: '1.0.0',
        })
      ),
    };

    await TestBed.configureTestingModule({
      declarations: [VersionInfoComponent],
      providers: [{ provide: VersionInfoService, useValue: versionInfoServiceMock }],
    }).compileComponents();

    fixture = TestBed.createComponent(VersionInfoComponent);
    component = fixture.componentInstance;
    versionInfoService = TestBed.inject(VersionInfoService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadBackendServerInfo on the versionInfoService when ngOnInit is called', () => {
    component.ngOnInit();
    expect(versionInfoService.loadBackendServerInfo).toHaveBeenCalled();
  });

  it('should set currVersion when loadBackendServerInfo is successful', () => {
    const expectedVersionInfo: VersionInfo = {
      frontendVersion: Globals.FRONTEND_VERSION,
      backendVersion: '1.0.0',
      dbVersion: '1.0.0',
    };
    component.ngOnInit();
    expect(component.currVersion).toEqual(expectedVersionInfo);
  });
});
