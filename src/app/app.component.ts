/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionContext } from '@common/session-context';
import { Settings } from '@model/settings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'app works!';

  constructor(
    private http: HttpClient,
    public router: Router,
    public sessionContext: SessionContext
  ) {
    this.http
      .get<Settings>('assets/settings.json')
      .subscribe((settings: Settings) => (this.sessionContext.settings = settings));

    this.sessionContext.centralHttpResultCode$.subscribe((rc: number) => {
      this.onRcFromHttpService(rc);
    });
  }

  private onRcFromHttpService(rc: number): void {
    if (rc === 401) {
      this.router.navigate(['/login']);
    }
  }
}
