variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: default
  CACHE_COMPRESSION_LEVEL: default
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  CLI_VERSION: 16.2.9

image: node:20.10.0-alpine3.18


#-----------------------------------------------------------------------------------------------------------------------
stages:
  - build
  #- test
  #- sonarqube
  - dockerimage-ext-job


#-----------------------------------------------------------------------------------------------------------------------
.cache:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Cache
  cache:
    key:
      files:
        - package-lock.json
      prefix: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/


#-----------------------------------------------------------------------------------------------------------------------
.install_dependencies:
#-----------------------------------------------------------------------------------------------------------------------
  stage: Install_dependencies
  extends: .cache
  before_script:
    - echo "Pipeline ID = $CI_PIPELINE_ID"
    - echo "Project name = $CI_PROJECT_NAME"
    - echo "Build ref = $CI_BUILD_REF_NAME"
    - echo "CI_COMMIT_REF_SLUG = $CI_COMMIT_REF_SLUG"
    - echo "CI_PROJECT_DIR = $CI_PROJECT_DIR"
    - test -d "node_modules" && echo "Found/Exists" || echo "Does not exist"
    - |
      if [[ ! -d node_modules ]]; then
        npm ci --cache .npm --prefer-offline
      fi

#-----------------------------------------------------------------------------------------------------------------------
build:
#-----------------------------------------------------------------------------------------------------------------------
  stage: build
  extends: .install_dependencies
  script:
    - npm run build
    - printenv
    - |
      if [[ "$CI_COMMIT_TAG" == "" ]]; then
        echo "ARTIFACTS_DOWNLOAD_REF=$CI_COMMIT_BRANCH" > variables.env
      else
        echo "ARTIFACTS_DOWNLOAD_REF=$CI_COMMIT_TAG" > variables.env
      fi
    - echo CI_COMMIT_BRANCH $CI_COMMIT_BRANCH
    - echo ARTIFACTS_DOWNLOAD_REF ${ARTIFACTS_DOWNLOAD_REF}
    - echo current CI_COMMIT_TAG $CI_COMMIT_TAG
  artifacts:
    paths:
      - dist/
    reports:
      dotenv: variables.env
  only:
    - main
    - master
    - develop
    - tags


#-----------------------------------------------------------------------------------------------------------------------
.test:karma:
#-----------------------------------------------------------------------------------------------------------------------
  stage: test
  extends: .cache
  image: trion/ng-cli-karma:${CLI_VERSION}
  allow_failure: false
  script:
    - npm run test:ci
  coverage: '/Lines \W+: (\d+\.\d+)%.*/'
  artifacts:
    paths:
      - coverage/
  only:
    - main
    - master
    - develop
    - tags


#-----------------------------------------------------------------------------------------------------------------------
.sonarqube:
#-----------------------------------------------------------------------------------------------------------------------
  stage: sonarqube
  image:
    name: sonarsource/sonar-scanner-cli:4.6
    entrypoint: [""]
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .sonar/cache
  script:
    - echo ${CI_PROJECT_DIR}
    - sonar-scanner -Dsonar.qualitygate.wait=true -Dsonar.branch.name="${CI_COMMIT_REF_NAME}"
  allow_failure: true
  dependencies:
    - test:karma
  only:
    - main
    - master
    - develop
    - tags


#-----------------------------------------------------------------------------------------------------------------------
# external Stage (Dockerimage)
#-----------------------------------------------------------------------------------------------------------------------
staging-external:
  stage: dockerimage-ext-job
  trigger: openkonsequenz/qa/portal-build-job
  variables:
    ARTIFACTS_DOWNLOAD_REF: $ARTIFACTS_DOWNLOAD_REF
    CI_COMMIT_TAG_CHILD: $CI_COMMIT_TAG
    CI_COMMIT_SHORT_SHA_CHILD: $CI_COMMIT_SHORT_SHA
  needs:
    - build
  only:
    - main
    - master
    - develop
    - tags
